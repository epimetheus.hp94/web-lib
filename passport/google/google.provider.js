const GoogleStrategy = require('passport-google-oauth20').Strategy;

exports.register = async (container) => {
    container.singleton('passport.google', async () => {
        let kernel   = await container.make('http.kernel');
        let url      = await container.make('url');
        let config   = await container.make('config');
        let googleConfig = config.passport.google;

        return new GoogleStrategy({
            clientID: googleConfig.id,
            clientSecret: googleConfig.secret,
            callbackURL: url.route(googleConfig.callback),
            passReqToCallback: true,
            scope: ['profile']
        }, (request, accessToken, refreshToken, profile, callback) => {
            kernel.context.google = {
                request: request,
                accessToken: accessToken,
                refreshToken: refreshToken,
                profile: profile
            };

            callback(null, profile);
        });

    })
};


exports.boot = async (container) => {
    let router   = await container.make('http.router');
    let passport = await container.make('passport');
    let config   = await container.make('config');

    passport.use((await container.make('passport.google')));

    router.get(config.passport.facebook.url || '/auth/google', async(context, next) => {
        return passport.authenticate('google')(context, next);
    });

};

