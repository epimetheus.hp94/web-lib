const FacebookStrategy = require('passport-facebook');

exports.register = async (container) => {
    container.singleton('passport.facebook', async () => {
        let kernel   = await container.make('http.kernel');
        let url      = await container.make('url');
        let config   = await container.make('config');
        let facebookConfig = config.passport.facebook;

        return new FacebookStrategy({
            clientID: facebookConfig.id,
            clientSecret: facebookConfig.secret,
            callbackURL: url.route(facebookConfig.callback),
            profileFields: facebookConfig.scope || 'public_profile'
        }, (accessToken, refreshToken, profile, callback) => {
            kernel.context.facebook = {
                accessToken: accessToken,
                refreshToken: refreshToken,
                profile: profile
            };

            callback(null, profile);
        });

    })
};


exports.boot = async (container) => {
    let router   = await container.make('http.router');
    let passport = await container.make('passport');
    let config   = await container.make('config');

    passport.use((await container.make('passport.facebook')));

    router.get(config.passport.facebook.url || '/auth/facebook', async(context, next) => {
        return passport.authenticate('facebook')(context, next);
    });

};

