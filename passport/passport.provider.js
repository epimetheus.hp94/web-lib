const passport = require('passport');

exports.register = async (container) => {
    container.value('passport', passport);
};
