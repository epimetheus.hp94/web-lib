const VError = require('verror');
const multer = require('multer');
const path   = require('path');
const uuid   = require('uuid/v4');

class UploaderFactory {

    make(adapter, config) {
        switch (adapter) {
            case 'disk':
                return this.makeDiskAdapter(config);
            case 'memory':
                return this.makeMemoryAdapter(config);
            default :
                throw new VError(
                    `E_UPLOADER: Adapter [${adapter}] is not supported`);
        }
    }

    makeDiskAdapter(config) {
        return multer.diskStorage({
            destination: function(req, file, cb) {
                cb(null, path.normalize(path.join(
                    config.destination
                )));
            },
            filename   : function(req, file, cb) {

                let today            = new Date();
                let filename         = uuid();
                let filenameWithDate = `${today.toLocaleDateString()}--${filename}`;
                let fileNameSegments = file.originalname.split('.');
                let fileExt          = fileNameSegments.pop();

                file.meta = {
                    name    : filename,
                    fullName: `${filenameWithDate}.${fileExt}`,
                    url     : config.url.replace(/{name}/g, filenameWithDate).
                        replace(/{ext}/g, fileExt)
                };

                cb(null, `${filenameWithDate}.${fileExt}`);
            }
        });
    }

    makeMemoryAdapter(config) {

    }
}

module.exports = UploaderFactory;