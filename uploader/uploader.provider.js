const koaMulter       = require('koa-multer');
const UploaderFactory = require('./uploader.factory');

exports.register = async (container) => {
    container.singleton('uploader.storageFactory', async () => {
        return new UploaderFactory();
    });
};

exports.boot = async (container) => {
    let router          = await container.make('http.router');
    let config          = await container.make('config');
    let uploaderFactory = await container.make('uploader.storageFactory');

    let adapterConfig = config.upload.storages[config.upload.use];

    const upload = koaMulter({
        storage: uploaderFactory.make(adapterConfig.adapter, adapterConfig)
    });

    router.post(config.upload.url,
        upload.single(config.upload.singleName),
        async (context) => context.body = context.req.file.meta
    );
};