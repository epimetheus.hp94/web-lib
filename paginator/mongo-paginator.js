const PaginatedCollection = require('./paginated-collection');
const VError              = require('verror');

class MongoPaginator {
    /**
     * Set the maximum number of item in a page
     *
     * @param itemPerPage
     * @returns {MongoPaginator}
     */
    setItemPerPage(itemPerPage) {
        this.itemPerPage = itemPerPage;

        return this;
    }

    /**
     *
     * @param {number} page
     * @param {number} itemPerPage
     * @returns {Promise<PaginatedCollection>}
     */
    async fetch(query, page = 1, itemPerPage = null) {

        if (page <= 0) {
            throw new VError('E_PAGINATOR: Invalid page [%s]', page);
        }

        if (null === itemPerPage) {
            itemPerPage = this.itemPerPage;
        }

        let startIndex    = (page - 1) * itemPerPage;
        let rawCollection = await query.skip(startIndex).limit(itemPerPage + 1).toArray();
        let hasNext       = rawCollection.length > itemPerPage;

        if (hasNext) {
            rawCollection.pop();
        }

        let collection = new PaginatedCollection(rawCollection)
            .setCurrentPage(page)
            .setItemPerPage(itemPerPage)
        ;

        if (hasNext) {
            collection.havingNext();
        }

        return collection;
    }
}

module.exports = MongoPaginator;
