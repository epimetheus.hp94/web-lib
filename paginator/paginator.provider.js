const DatabasePaginator = require('./database-paginator');
const MongoPaginator    = require('./mongo-paginator');

module.exports.register = async (container) => {
    container.singleton('paginator.database', async () => {
        let config = await container.make('config');

        return new DatabasePaginator().setItemPerPage(config.paginator.itemPerPage);
    });

    container.singleton('mongo.paginator', async () => {
        let config = await container.make('config');

        return new MongoPaginator().setItemPerPage(config.paginator.itemPerPage);
    })
};
